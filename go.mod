module modernc.org/readline

go 1.15

require (
	github.com/chzyer/logex v1.1.10 // indirect
	github.com/chzyer/readline v0.0.0-20180603132655-2972be24d48e
	github.com/chzyer/test v0.0.0-20180213035817-a1ea475d72b1
	github.com/nbutton23/zxcvbn-go v0.0.0-20180912185939-ae427f1e4c1d
	github.com/stretchr/testify v1.6.1 // indirect
	golang.org/x/sys v0.0.0-20201101102859-da207088b7d1
)
